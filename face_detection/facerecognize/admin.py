from django.contrib import admin
from .models import Face_Image, PostImage
# Register your models here.

class PostImageAdmin(admin.StackedInline):
    model = PostImage
 
@admin.register(Face_Image)
class PostAdmin(admin.ModelAdmin):
    inlines = [PostImageAdmin]
 
    class Meta:
       model = Face_Image
 
@admin.register(PostImage)
class PostImageAdmin(admin.ModelAdmin):
    pass