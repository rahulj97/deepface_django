from os import path
import pickle
from deepface import DeepFace
import cv2
import numpy as np
from retinaface import RetinaFace
from deepface import DeepFace
from skimage import data, io, filters

def show_image(faces,image,folder):
  rectangle = ""
  rect_image = image
  color = []
  for face in faces:
    left,top,right,bottom = get_locations(faces,face)
    # You can access the actual face itself like this:
    face_image = image[top:bottom, left:right]
    color.append(face_image)

  '''for face in faces:
    left,top,right,bottom = get_locations(faces,face)
    rectangle = cv2.rectangle(rect_image, (left, top), (right, bottom), (0, 0, 255), 2)
  color = [rectangle]'''

  return color

def get_locations(faces,face):
    facial_area = faces[face]['facial_area']
    # Print the location of each face in this image
    left = facial_area[0]
    top = facial_area[1]
    right = facial_area[2]
    bottom = facial_area[3]
    return left,top,right,bottom

    
def face_detection(image):
    try:
      resp = RetinaFace.detect_faces(image,720)
      if type(resp) == tuple:
          resp = RetinaFace.detect_faces(image,1280)
      color = show_image(resp,image ,"Cropped")
      return color
    except:
     return False
     
def face_recognition(image):
  try:
   df = DeepFace.find(img_path =image, db_path = "media\Cropped", enforce_detection= False)
   images = []
   for i in range(len(df)):
      images.append(df["identity"].iloc[i])
  except:
    images = "No images"
  print(images)
  return images
    

def deep_representation(img):
  img = "media\\Cropped/" + img.split('/')[3]
  if (isinstance(img, np.ndarray) and img.any()): #numpy array
    image = img.copy()
  else:
    image = img

    model_name = 'VGG-Face'
    model = DeepFace.build_model(model_name)
    models = {}
    models[model_name] = model
    db_path = "media\Cropped"
    file_name = "representations_%s.pkl" % (model_name)
    file_name = file_name.replace("-", "_").lower()
    representation = []

    if path.exists(db_path+"/"+file_name):
      f = open(db_path+'/'+file_name, 'rb')
      representations = pickle.load(f)
      custom_model = models[model_name]
      instance = []
      present =False
      representation = (DeepFace.represent(img_path = image, model_name = model_name, model = custom_model, enforce_detection = False, detector_backend = 'opencv', align = True, normalization = 'base'))
      for r in representations:
        if r[1] == representation:
          present = True
          break
      print(present)
      if present == False:
        instance.append(image)
        instance.append(representation)
        representations.append(instance)
        f = open(db_path+'/'+file_name, "wb")
        pickle.dump(representations, f)
        f.close()