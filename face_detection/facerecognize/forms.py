from django import forms
from django.forms import fields, widgets
from .models import Face_Image

class ImageForm(forms.ModelForm):
    class Meta:
        model = Face_Image
        fields = ('img',)