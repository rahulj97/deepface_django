from django.db import models
from django.db.models.deletion import DO_NOTHING

# Create your models here.

class Face_Image(models.Model):
    name= models.CharField(max_length=100)
    img= models.ImageField(upload_to='Search_Images')
    def __str__(self):
        return str(self.id)
    
    

class PostImage(models.Model):
    post = models.ForeignKey(Face_Image,related_name="posts",default=None, on_delete=models.CASCADE)
    images = models.FileField(upload_to = 'Cropped/')
 
    def __str__(self):
        return str(self.id)