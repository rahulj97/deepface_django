from rest_framework import serializers
from .models import Face_Image, PostImage

class faceSerializer(serializers.Serializer):
    name= serializers.CharField(max_length=100)
    img= serializers.ImageField()
    