from django.urls import path

from . import views

urlpatterns = [
    path('',views.index,name='home'),
    path('detect_face',views.face_detect, name="detection")
]