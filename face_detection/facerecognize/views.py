from django.shortcuts import redirect, render
from .forms import ImageForm
from .models import Face_Image, PostImage
from .face_recognition import face_recognition,face_detection, deep_representation
from PIL import Image
import numpy as np
from io import BytesIO
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from rest_framework.decorators import api_view
import time

# Create your views here.

def index(request):
       form=ImageForm()
       return render(request,"home.html",{"form":form})

def face_detect(request):
    start_time = time.time()
    if request.method == 'POST':
        form = ImageForm(data=request.POST,files=request.FILES)
        if form.is_valid():
            image = form.cleaned_data['img']
            print(image)
            image_obj = Face_Image.objects.create(img=image) #create will create as well as save too in db.
            pil_image = Image.open(image)
            cv_img = np.array(pil_image)
            imgs = face_detection(cv_img)
            if imgs == False:
                return render(request,"home.html",{"alert":"Face not found"})
            else:
                posts = []
                face = {}
                for img in imgs:
                        im_pil = Image.fromarray(img)
                        im_np = np.asarray(im_pil)
                        buffer= BytesIO()
                        im_pil.save(buffer, format ="png")
                        image_png = buffer.getvalue()
                        content = ContentFile(image_png)
                        face = face_recognition(img)
                        image_file = InMemoryUploadedFile(content, None, 'face.jpg', 'image/jpeg', content.tell, None)
                        post = PostImage.objects.create(post=image_obj,images=image_file)
                        posts.append(post)
                        time.sleep(2)
                        deep_representation(post.images.url)
                timer  = time.time() - start_time
                return render(request,"home.html",{"obj":image_obj,"posts":posts,"time":timer,"faces":face})
    else:
        return redirect('home')